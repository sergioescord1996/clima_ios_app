//
//  WeatherManager.swift
//  Clima
//
//  Created by Sergio Escalante Ordonez on 17/09/2020.
//  Copyright © 2020 App Brewery. All rights reserved.
//

import Foundation
import CoreLocation

protocol WeatherManagerDelegate {
    func didUpdateWeather(_ weatherManager: WeatherManager, weather: WeatherModel)
    func didFailWithError(_ error: Error)
}

struct WeatherManager {
    
    let mainUrl = "https://api.openweathermap.org/data/2.5/weather?"
    let appId = "7e64e49b1e1ab1cc72077c415ca1260b"
    
    var delegate: WeatherManagerDelegate?
    
    func fetchWeather(_ city: String, units: String? = "metric") {
        let urlString = "\(mainUrl)appid=\(appId)&q=\(city)&units=\(units!)"
        performRequest(with: urlString)
    }
    
    func fetchWeather(latitude: CLLocationDegrees, longitude: CLLocationDegrees, units: String? = "metric") {
        let urlString = "\(mainUrl)appid=\(appId)&lat=\(latitude)&lon=\(longitude)&units=\(units!)"
        performRequest(with: urlString)
    }
    
    func performRequest(with urlString: String) {
        
        guard let url = URL(string: urlString) else { return }
        
        let session = URLSession(configuration: .default)
        
        let task = session.dataTask(with: url) { (data, response, error) in
            if let error = error {
                self.delegate?.didFailWithError(error)
            }
            
            if let safeData = data {
                if let weatherModel = self.parseJSON(safeData) {
                    self.delegate?.didUpdateWeather(self, weather: weatherModel)
                }
            }
        }
//        let task = session.dataTask(with: url, completionHandler: handler(data:urlResponse:error:))
        
        task.resume()
    }
    
//    func handler(data: Data?, urlResponse: URLResponse?, error: Error?) -> Void {
//        if let error = error {
//            print(error)
//        }
//
//        if let safeData = data {
//            let dataString = String(data: safeData, encoding: .utf8)
//            print(dataString!)
//        }
//    }
    
    func parseJSON(_ weatherData: Data) -> WeatherModel? {
        let decoder = JSONDecoder()
        
        do {
            let decodedData = try decoder.decode(WeatherData.self, from: weatherData)
//            print(getConditionName(weatherId: decodedData.weather[0].id))
            let weather = WeatherModel(conditionId: decodedData.weather[0].id,
                                       cityName: decodedData.name,
                                       temperature: decodedData.main.temp)
            
            return weather
        } catch {
            delegate?.didFailWithError(error)
            return nil
        }
    }
    
//    func getConditionName(weatherId: Int) -> String {
//       switch weatherId {
//        case 200...232:
//            return "cloud.bolt"
//        case 300...321:
//            return "cloud.drizzle"
//        case 500...531:
//            return "cloud.rain"
//        case 600...622:
//            return "cloud.snow"
//        case 701...781:
//            return "cloud.fog"
//        case 800:
//            return "sun.max"
//        case 801...804:
//            return "cloud.bolt"
//        default:
//            return "cloud"
//        }
//    }

}
